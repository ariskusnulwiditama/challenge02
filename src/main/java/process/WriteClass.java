package process;

import java.io.File;
import java.io.IOException;
import java.io.*;

public class WriteClass extends ProcessClass {
    private static String text = "E:\\kode\\challenge02\\src\\main\\temp\\data.txt";
    private static String group = "E:\\kode\\challenge02\\src\\main\\temp\\group.txt";
    private static String csvFile = "E:\\kode\\challenge02\\src\\main\\temp\\data_sekolah.csv";


//    public static void writeAll() {
//        writeMeanMedianModus();
//        writeGroup(int input);
//    }

    public static void writeGroup(int input) {
        try {
            File file = new File(group);
            if(!file.exists()) {
                file.createNewFile();
            }
            ProcessInterface processInterface = new ProcessClass();
            FileWriter fileWriter = new FileWriter(file.getAbsoluteFile());
            BufferedWriter bufferedWriter = new BufferedWriter(fileWriter);
            bufferedWriter.write("nilai yang lebih besar dari "+input);
            bufferedWriter.newLine();
            bufferedWriter.write(Integer.toString(Math.toIntExact(processInterface.groupHigher(csvFile, input))));
            bufferedWriter.newLine();
            bufferedWriter.write("nilai yang lebih kecil dari "+input);
            bufferedWriter.newLine();
            bufferedWriter.write(Integer.toString(Math.toIntExact(processInterface.groupLower(csvFile, input))));
            bufferedWriter.flush();
            bufferedWriter.close();
            System.out.println("sukses ditulis");
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public static void writeMeanMedianModus() {
        try {
            File file = new File(text);
            if(!file.exists()) {
                file.createNewFile();
            }
            ProcessInterface processInterface = new ProcessClass();
            FileWriter fileWriter = new FileWriter(file.getAbsoluteFile());
            BufferedWriter bufferedWriter = new BufferedWriter(fileWriter);
            bufferedWriter.write("nilai mean dari data siswa:");
            bufferedWriter.newLine();
            bufferedWriter.write(Double.toString(processInterface.generateMean(csvFile)));
            bufferedWriter.newLine();
            bufferedWriter.write("nilai midean dari data siswa:");
            bufferedWriter.newLine();
            bufferedWriter.write(Integer.toString(processInterface.generateMedian(csvFile)));
            bufferedWriter.newLine();
            bufferedWriter.write("nilai modus nilai siswa: ");
            bufferedWriter.newLine();
            bufferedWriter.write(Integer.toString(processInterface.generateModus(csvFile)));
            bufferedWriter.flush();
            bufferedWriter.close();
            System.out.println("sukses ditulis");
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

}
